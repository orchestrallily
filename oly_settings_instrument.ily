\version "2.17.6"
% lilypond settings for instrumental scores
% changes to the global score settings for single-instrument sheets
#(ly:set-option 'relative-includes #t)
\include "oly_settings_global.ily"


\paper {
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  % Allow line-breaks only at rests!
  #(define page-breaking ly:page-turn-breaking)
  % Prevent blank pages inside the score
  blank-page-penalty = #100000
  blank-after-score-page-penalty = #100000
  left-margin = 1.5\cm
  right-margin = 1.5\cm
  line-width = 18\cm
}

\layout {
  \context {  \Staff
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
  }
  \context {  \StaffGroup
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
  }
  \context {  \ChoirStaff
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
  }
  \context {  \PianoStaff
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
  }
  \context {
    \Staff \RemoveEmptyStaves
    % No instrument names, they are already in the title/header! (Removing
    % the engraver only from the \Staff context is not sufficient!)
    \remove "Instrument_name_engraver"
  }

  \context { \Staff
    \consists "Page_turn_engraver"
    minimumPageTurnLength = #(ly:make-moment 9 8)
  }
}

