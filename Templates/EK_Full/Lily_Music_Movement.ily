\version "<< version >>"

<< id >>PieceName = "<$ if piece -$><< piece >><$- else -$><< id >><$- endif $>"
<< id >>PieceNameTacet = "
  <$- if piecetacet: -$><< piecetacet >>
  <$- elif piece-$> << piece >> tacet
  <$- else -$> << id >>
  <$- endif -$>"
<< id >>Tempo = "TODO"

<< id >>Key = \key c \major
<< id >>TimeSignature = \time 4/4

<< id >>Settings = \notemode {
  \tempo \<< id >>Tempo
}

<$ for i in instruments $>
<< id >><< i >>Music = \relative c' {
  s8
  % TODO
}
\addQuote "<< id >><< i >>Music" { \<< id >><< i >>Music }

<$ if i in vocalvoices -$>
<< id >><< i >>Lyrics = \lyricmode {
  % TODO
}

<$ endif $>
<$- endfor -$>
