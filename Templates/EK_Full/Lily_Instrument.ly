\version "<< version >>"
\include "<< basename >>_Settings_<< settings >>.ily"
<$ if createmidi: $>
\setCreateMIDI ##t
\setCreatePDF ##t
<$- endif $>
\header {
  instrument = \<< instrument >>InstrumentName
  instrumentnr = \<< instrument>>Number
}

<$ for p in parts -$>
\create<$ if nocues $>NoCues<$ endif $>Score #"<< p.id >>" #'("<< instrument >>")
<$ endfor $><< '' >>
