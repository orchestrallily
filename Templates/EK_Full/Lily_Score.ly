\version "<< version >>"
\include "<< basename >>_Settings_<< settings >>.ily"
<$ if fullscore: -$>
\setCreateMIDI ##t
\setCreatePDF ##t
<$- endif $>

\header {
  scoretype = \<< score >>Title
  instrumentnr = \<< score >>Number
}
<$ if score in ['OriginalScore'] -$>
SClef = \clef "soprano"
AClef = \clef "alto"
TClef = \clef "tenor"
<$ endif -$>
<$ for p in parts $>
\create<$ if nocues $>NoCues<$ endif $>Score #"<< p.id >>" #'("<< score >>")
<$- endfor $>
