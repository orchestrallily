\version "<< version >>"
\include "orchestrallily/oly_settings_<< settings >>.ily"
\include "<< basename >>_Settings.ily"
<$- if settings in ['instrument'] and 'optimal-breaking' in options $>

\paper {
  #(define page-breaking ly:optimal-breaking)
}<$ endif $>