\version "<< version >>"
#(ly:set-option 'point-and-click #f)
<$ if language $>
\include "<< language >>.ly"
<$ endif $>
\include "<< basename >>_Settings_Global.ily"
<$ for f in parts_files -$>
\include "<< f >>"
<$ endfor $>
