OUT=out

<$ for w in works -$>
base<< w.nr >> = << w.basename >>
SRCS<< w.nr >> = << [w.srcfiles.parts|join (" "), w.srcfiles.settings|join (" ")]|join(" ")|replace ( w.basename, "$(base" ~ w.nr ~ ")" ) >>
instruments<< w.nr >> = << w.instruments|join (" ") >>
scores<< w.nr >> = << w.scores|join (" ") >>
pdfs<< w.nr >> = $(instruments<<w.nr>>:%=$(OUT)/$(base<<w.nr>>)_Instrument_%.pdf) \
     $(scores<<w.nr>>:%=$(OUT)/$(base<<w.nr>>)_Score_%.pdf)

<$ endfor $>
<$ if latex -$>
texbase = << latex.basename >>
texscores = << latex.scores|join (" ") >>
texs = $(texscores:%=TeX_$(texbase)_Score_%.tex)
texpdfs = $(texs:%.tex=$(OUT)/%.pdf) \
     $(OUT)/TeX_$(texbase)_Score_Instruments.pdf
<$- endif $>

distfiles = $(instruments:%=$(OUT)/$(base)_Instrument_%.pdf) $(texpdfs)

<$ set jj = joiner(" ") -$>
additionalscores =<$ for s in additionalscores $> << jj() >>$(OUT)/<< s >>.pdf<$ endfor $>

.PHONY: all clean
all: $(OUT)<$ for w in works $> $(instruments<< w.nr >>) $(scores<< w.nr >>)<$ endfor $> $(additionalscores)<$ if latex $> tex<$ endif $>
<$ for w in works -$>
instruments<<w.nr>>: $(instruments<<w.nr>>)
scores<<w.nr>>: $(scores<<w.nr>>)
<$ endfor $>

clean:
	rm -rf $(OUT)

lily: <$ for w in works $>$(pdfs<< w.nr >>) <$ endfor $>$(additionalscores)

$(OUT) :
	mkdir -p $(OUT)

<$ for w in works -$>
$(instruments<< w.nr >>): % : $(OUT)/$(base<< w.nr >>)_Instrument_%.pdf $(SRCS<< w.nr >>)
$(scores<< w.nr >>): % : $(OUT)/$(base<< w.nr >>)_Score_%.pdf $(SRCS<< w.nr >>)
<$ endfor $>

$(OUT)/%.pdf : %.ly
	lilypond -o $(basename $@) $<

$(OUT)/%.mp3 : %.midi
	timidity -c /etc/timidity/fluidr3_gm.cfg -OwM $< -o $@

$(additionalscores): $(OUT)/%.pdf: %.ly
	lilypond -dbackend=eps -o $(basename $@) $<

<$ if latex -$>
tex: $(additionalscores) latex
latex: <$ for w in works $>$(pdfs<< w.nr >>) <$ endfor $>$(texpdfs)
latexpdf: $(texpdfs)

$(OUT)/TeX_%.pdf : TeX_%.tex
	TEXINPUTS=$(OUT):orchestrallily/:.//: xelatex -interaction=batchmode --output-directory=$(OUT)/ $<
<$- endif $>

zipdir = $(base)_Final
dist: $(distfiles)
	mkdir -p $(zipdir)
	cp -r $(distfiles) $(zipdir)
	rename 's/TeX_//' $(zipdir)/*
	rename 's/Score_(Full|Long)\./Score./' $(zipdir)/*
	zip -jr $(base).zip $(zipdir)
	rm -rf $(zipdir)


webshop: $(distfiles)
	@python ../../Diverses/Webshop/EditionKainhofer_CSVI_Generate.py "<< settings_file >>" $(distfiles)
