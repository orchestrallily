\version "<< version >>"

\include "orchestrallily/oly_settings_names.ily"


\header {
  scorenumberbase = "<< scorenumber >>"
  title = "<< title >>"
  titlepagetitle = "<< title >>"
  subtitle = "<< subtitle >>"
  titlepagesubtitle = "<< subtitle >>"
  composer = "<< composer >><$ if composerdate $> (<< composerdate >>)<$ endif $>"
  shortcomposer = "<<composer>>"
<$- if arranger $>
  arranger = "<< arranger >><$ if arrangerdate $> (<<arrangerdate >>)<$ endif $>"<$ endif $>
  copyright = \markup \abs-fontsize #9 \column{
      \line { << year >>,
         <$- if publisherurl $> \with-url #"<< publisherurl >>" {<< publisher >>,}
	 <$- else $><< publisher >>, <$ endif $> \concat{\fromproperty #'header:scorenumber .} Alle Rechte vorbehalten / All rights reserved / Printed in Austria. }
%       \line {"Die Ausgabe darf kopiert und ohne Einschränkungen aufgeführt werden. / May be copied and performed without restriction."}
  }
%  ismn = "..."
}

%% Possibly adjust the \orchestralScoreStructure here...

%% Add custom keys here, e.g.
%% CorKey = \key g \major
%% CorIKey = \CorKey
%% CorIIKey = \CorKey
