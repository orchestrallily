#(set-default-paper-size "a4")
#(ly:set-option 'point-and-click #f)

\include "deutsch.ly"

\include "orchestrallily/oly_settings_vocalvoice.ily"
\include "Reutter_EcceQuomodo_Settings_Global.ily"
\include "Reutter_EcceQuomodo_Music_EcceQuomodo.ily"

#(set-global-staff-size 15)