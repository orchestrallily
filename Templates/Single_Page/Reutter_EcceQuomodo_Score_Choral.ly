\version "2.13.1"
\include "Reutter_EcceQuomodo_Settings.ily"
\setCreateMIDI ##t
\setCreatePDF ##t

\header {
  scoretype = \ChoralScoreTitle
  scorenumber = \markup{\scoreNumber \ChoralScoreNumber}
}

\createScore #"EcceQuomodo" #'("ChoralScore")

