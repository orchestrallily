\version "2.13.1"

\include "orchestrallily/orchestrallily.ily"
\include "orchestrallily/oly_settings_names.ily"

scorenumber="EK-0055"

\header { 
  title = "Ecce quomodo"
  subtitle = "Responsorium für den Karfreitag / Responsory for Good Friday"
  composer = "Georg Reutter (1708-1772)"
  copyright = \markup \abs-fontsize #9 \column{
      \line {2009, \concat{\with-url #"http://www.edition-kainhofer.com/" {Edition Kainhofer},} \concat{\scorenumber,} Vienna, Austria. 
      Lizensiert unter / Licensed under: Creative Commons BY \with-url #"http://creativecommons.org/licenses/by/3.0/at/" {\translate #'(0 . -0.7) \epsfile #Y #3 #"cc-by-2.eps" }}
      \line {Die Ausgabe darf kopiert und ohne Einschränkungen aufgeführt werden. / May be copied and performed without restriction.}
  }
}

% Add custom keys here, e.g.
% CorKey = \key g \major
% CorIKey = \CorKey
% CorIIKey = \CorKey
SShortInstrumentName = ##f
AShortInstrumentName = ##f
TShortInstrumentName = ##f
BShortInstrumentName = ##f

\paper {
  top-margin = 1\cm
  left-margin = 1.7\cm
  right-margin = 1.5\cm
  line-width = 17.8\cm
}
\layout {
  \context {  \Staff
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
  }
  \context {
    \PianoStaff
    \remove "Instrument_name_engraver"
  }
  \context {
    \ChoirStaff
    \remove "Instrument_name_engraver"
  }

  \context {
    \Score 
    \override StaffGrouper #'between-staff-spacing = #'((stretchability . 6) (padding . 1 ))
  }
}

\paper {
  top-system-spacing = #'((space . 7) (stretchability . 7))
  top-title-spacing = #'((space . 0) (stretchability . 1))
  after-title-spacing = #'((space . 7) (stretchability . 8))
  between-system-spacing = #'((space . 7) (stretchability . 7))
  bottom-system-spacing = #'((space . 7) (minimum-distance . 0) (padding . 0) (stretchability . 7))

%   top-system-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
%   top-title-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
%   between-system-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
%   bottom-system-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
%   after-title-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
%   before-title-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
%   between-title-spacing = #'((space . 0) (minimum-distance . 0) (padding . 0) (stretchability . 0))
}