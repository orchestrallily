\version "<< version >>"
\include "<< basename >>_Settings.ily"
\setCreateMIDI ##t
\setCreatePDF ##t

\header {
  scoretype = \<< score >>Title
  scorenumber = \markup{\scoreNumber \<< score >>Number}
}
<$ if score is sameas "Original" $>
SClef = \clef "soprano"
AClef = \clef "alto"
TClef = \clef "tenor"
<$ endif $>
<$ for p in parts $>
\create<$ if nocues $>NoCues<$ endif $>Score #"<< p.id >>" #'("<< score >>")
<$- endfor $>
