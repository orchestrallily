#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import os.path
import getopt
import re
import filecmp
import string
import subprocess
import copy
import codecs
import fnmatch
from jinja2 import Environment, Template, FileSystemLoader
import jinja2

import pprint
pp = pprint.PrettyPrinter(indent=4)

program_name = 'generate_oly_score';
settings_file = 'oly_structure.def';
script_path = os.path.dirname(__file__);

######################################################################
#   Options handling
######################################################################

help_text = r"""Usage: %(program_name)s [OPTIONS]... [DEF-FILE]
Create a complete file structure for a score using OrchestralLily.
If no definitions file it given, the file oly_structure.def is used

Options:
 -h, --help         print this help
 -o, --output=DIR   write output files to DIR (default: read from settings file)
"""

def help (text):
    sys.stdout.write (text)
    sys.exit (0)




def import_file (filename):
  res={}
  try:
    in_f = codecs.open (filename, "r", "utf-8")
    s = in_f.read()
    in_f.close()
    res = eval(s);
  except IOError:
    print ("Unable to load file '%s'. Exiting..." % filename)
    exit (-1);
  except SyntaxError as ex:
    print ex;
    print ("Unable to interpret settings file '%s', it's syntax is invalid. Exiting..." % filename);
    exit (-1);
  return res


# Load the score type/number definitions from the oly directory
score_types = import_file (os.path.join(sys.path[0], "oly_defs.py"));



######################################################################
#   Settings
######################################################################

class Settings:
  options = {};
  arguments = [];
  globals={};
  raw_data={};
  out_dir = "";
  template_env = None;

  def __init__ (self):
    settings_file = self.load_options ();
    self.load (settings_file);
    self.raw_data["settings_file_path"] = settings_file;
    self.raw_data["settings_file"] = os.path.basename(settings_file);
    self.init_template_env ();
    self.init_arrays ();

  def output_dir (self):
    return self.out_dir
  def defaults (self):
    return self.raw_data.get ("defaults", {});
  def score_names (self):
    return self.raw_data.get ("scores", ["Score"]);

  def get_score_settings (self, id):
    settings = self.globals.copy();
    settings.update (self.defaults ());
    settings.update ({"name": id})
    settings.update (self.raw_data.get (id, {}));
    self.normalize_part_definitions (settings);
    return settings;

  def normalize_part_definitions (self, score_settings):
    scorename = score_settings.get ("name", "Score");
    parts = score_settings.get ("parts", [scorename]);
    result = [];
    for this_part in parts:
      if isinstance (this_part, basestring):
        this_part = {"id": this_part, "filename": this_part }
      elif not isinstance (this_part, dict):
        warning ("Invalid part list for score %a: %a" % (scorename, p));
        return [];
      if "id" not in this_part:
        this_part["id"] = scorename;
      if "filename" not in this_part:
        this_part["filename"] = this_part["id"];
      if "piece" not in this_part:
        this_part["piece"] = this_part["id"];
      this_part["score"] = scorename;
      this_part.update (score_settings);
      result.append (this_part);
    score_settings["parts"] = result;

  def has_tex (self):
    return "latex" in self.raw_data;
  def get_tex_settings (self):
    settings = self.globals.copy();
    settings.update (self.defaults ());
    settings.update (self.raw_data.get ("latex", {}));
    return settings;

  def get_score_parts (self, score_settings):
    scorename = score_settings.get ("name", "Score");
    parts = score_settings.get ("parts", [scorename]);
    result = [];
    for p in parts:
      this_part = p;
      if isinstance (this_part, basestring):
        this_part = {"id": this_part, "filename": this_part }
      elif not isinstance (this_part, dict):
        warning ("Invalid part list for score %a: %a" % (scorename, p));
        return [];
      if "id" not in this_part:
        this_part["id"] = scorename;
      if "filename" not in this_part:
        this_part["filename"] = this_part["id"];
      if "piece" not in this_part:
        this_part["piece"] = this_part["id"];
      this_part["score"] = scorename;
      this_part.update (score_settings);
      result.append (this_part);
    return result;

  def load_options (self):
    (self.options, self.arguments) = getopt.getopt (sys.argv[1:], 'ho:', ['help', 'output='])
    for opt in self.options:
      o = opt[0]
      a = opt[1]
      if o == '-h' or o == '--help':
        help (help_text % globals ())
      elif o == '-o' or o == '--output':
        self.out_dir = a
      else:
        raise Exception ('unknown option: ' + o)
    if self.arguments:
      return self.arguments[0]
    else:
      return "oly_structure.def";

  def load (self, filename):
    self.raw_data = import_file (filename)
    if not self.out_dir:
      self.out_dir = self.raw_data.get ("output_dir", self.out_dir) + "/";

  def get_template_name (self):
    return self.raw_data.get ("template", "Full");
  def get_template_names (self, pattern):
    allfiles = os.listdir (self.templatepath);
    files = [];
    for f in allfiles:
      if fnmatch.fnmatch(f, pattern):
        files.append (f);
    return files;


  def init_template_env (self):
    global program_name;
    global script_path;
    templatename = self.get_template_name ();
    self.defaulttemplatepath = script_path + '/Templates/default';
    self.templatepath = script_path + '/Templates/' + templatename;
    self.template_env = Environment (
          loader = FileSystemLoader([self.templatepath,self.defaulttemplatepath]),
          block_start_string = '<$', block_end_string = '$>',
          variable_start_string = '<<', variable_end_string = '>>',
          comment_start_string = '<#', comment_end_string = '#>',
          #line_statement_prefix = '#'
    );
  def get_template (self, template):
    return self.template_env.get_template (template);


  def init_arrays (self):
    # Globals
    self.globals = self.raw_data.copy ();
    del self.globals["defaults"];
    if "latex" in self.globals:
      del self.globals["latex"];
    for i in self.globals.get ("scores", []):
      if i in self.globals:
        del self.globals[i];

  def assemble_filename (self, base, id, name, ext):
    parts = [base, id, name];
    if "" in parts:
      parts.remove ("")
    return "_".join (parts) + "." + ext;
  def assemble_movement_filename (self, basename, mvmnt):
    return self.assemble_filename( basename, "Music", mvmnt, "ily");
  def assemble_instrument_filename (self, basename, instr):
    return self.assemble_filename( basename, "Instrument", instr, "ly");
  def assemble_score_filename (self, basename, instr):
    return self.assemble_filename( basename, "Score", instr, "ly");
  def assemble_settings_filename (self, basename, s):
    return self.assemble_filename( basename, "Settings", s, "ily");
  def assemble_itex_filename (self, basename, filename):
    return self.assemble_filename( "TeX", basename, filename, "itex");
  def assemble_texscore_filename (self, basename, score):
    return self.assemble_filename( "TeX", basename, "Score_" + score, "tex");

######################################################################
#   File Writing
######################################################################

def write_file (path, fname, contents):
  file_name = path + fname;
  fn = file_name
  if os.path.exists (file_name):
    fn += ".new"

  dir = os.path.dirname (fn);
  if not os.path.exists (dir):
    os.mkdir (dir)

  try:
    out_f = codecs.open (fn, "w", "utf-8")
    s = out_f.write(contents)
    out_f.write ("\n")
    out_f.close()
  except IOError:
    print ("Unable to write to output file '%s'. Exiting..." % fn)
    exit (-1);

  # If the file already existed, check if the new file is identical.
  if (fn != file_name):
    patchfile = os.path.join (dir, "patches", os.path.basename(file_name) + ".patch")
    if os.path.exists (patchfile):
      try:
        retcode = subprocess.call("patch -Ns \""+ fn+ "\" \"" +  patchfile + "\"", shell=True)
        if retcode < 0:
          print >>sys.stderr, "Unable to apply patch to file \"", fn, "\"."
      except OSError, e:
        print >>sys.stderr, "Execution failed:", e

    if filecmp.cmp (fn, file_name):
      os.unlink (fn);
    else:
      print ("A file %s already existed, created new file %s." % (os.path.basename(file_name), os.path.basename(fn)))



######################################################################
#   Creating movement files
######################################################################


def generate_movement_files (score_name, score_settings, settings):
  parts_files = [];
  for part_settings in settings.get_score_parts (score_settings):
    template = settings.get_template ("Lily_Music_Movement.ily");
    basename = part_settings.get ("basename", score_name);
    filename = part_settings.get ("filename", score_name + "Part");
    filename = settings.assemble_movement_filename (basename, filename);
    write_file (settings.out_dir, filename, template.render (part_settings));
    parts_files.append (filename);

  return parts_files;



######################################################################
#   Creating instrumental score files
######################################################################

def generate_instrument_files (score_name, score_settings, settings):
  instrument_files = [];
  settings_files = set ();
  instrument_settings = score_settings.copy ();
  instrument_settings["parts"] = settings.get_score_parts (score_settings);

  template = settings.get_template ("Lily_Instrument.ly");
  basename = score_settings.get ("basename", score_name );

  noscore_instruments = score_settings.get ("noscore_instruments", [])
  for i in score_settings.get ("instruments", []):
    if i in noscore_instruments:
      continue;
    instrument_settings["instrument"] = i;
    if i in score_settings.get ("vocalvoices"):
      instrument_settings["settings"] = "VocalVoice";
    else:
      instrument_settings["settings"] = "Instrument";
    settings_files.add (instrument_settings["settings"]);
    filename = settings.assemble_instrument_filename (basename, i);
    write_file (settings.out_dir, filename, template.render (instrument_settings));
    instrument_files.append (filename);

  return (instrument_files, settings_files);



######################################################################
#   Creating score files
######################################################################


score_name_map = {
 "Particell": "Particell",
}
settings_map = {
 "OrganScore": "VocalScore",
 "ChoralScore": "ChoralScore",
 "LongScore": "FullScore",
 "OriginalScore": "FullScore",
 "Particell": "FullScore"
}
scores_cues = ["ChoralScore", "VocalScore"];

def generate_score_files (score_name, score_settings, settings):
  score_files = [];
  settings_files = set ();
  s_settings = score_settings.copy ();
  s_settings["parts"] = settings.get_score_parts (score_settings);
  s_settings["fullscore"] = True;

  template = settings.get_template ("Lily_Score.ly");
  basename = score_settings.get ("basename", score_name );

  for s in score_settings.get ("scores", []):
    fullsn = score_name_map.get (s, (s + "Score"));
    s_settings["score"] = fullsn;
    s_settings["nocues"] = fullsn not in scores_cues;
    s_settings["settings"] = settings_map.get (fullsn,fullsn)
    settings_files.add (s_settings["settings"]);
    filename = settings.assemble_score_filename (basename, s);
    write_file (settings.out_dir, filename, template.render (s_settings));
    score_files.append (filename);

  return (score_files, settings_files);



######################################################################
#   Creating settings files
######################################################################

def write_settings_file_if_exists (settings, score_settings, template, filename):
  try:
    template = settings.get_template (template);
    basename = score_settings.get ("basename", "");
    filename = settings.assemble_settings_filename (basename, filename);
    write_file (settings.out_dir, filename, template.render (score_settings));
    return filename;
  except jinja2.exceptions.TemplateNotFound as e:
    return None;

def generate_settings_files (score_name, score_settings, settings, settings_files):
  out_files = [];
  out_files.append (write_settings_file_if_exists (settings, score_settings, "Lily_Settings_Global.ily", "Global" ));
  out_files.append (write_settings_file_if_exists (settings, score_settings, "Lily_Settings.ily", "" ));

  for s in settings_files:
    score_settings["settings"] = s.lower ();
    out_files.append (write_settings_file_if_exists (settings, score_settings, "Lily_Settings_Generic.ily", s));

  return out_files;



######################################################################
#   Generation of Lilypond Files
######################################################################

def generate_scores (settings):
  files = {}
  for s in settings.score_names ():
    score_settings = settings.get_score_settings (s);
    parts_files = generate_movement_files (s, score_settings, settings);
    (instrument_files, isettings_files) = generate_instrument_files (s, score_settings, settings);
    (score_files, ssettings_files) = generate_score_files (s, score_settings, settings);
    included_settings_files = ssettings_files | isettings_files;
    score_settings["parts_files"] = parts_files;
    settings_files = generate_settings_files (s, score_settings, settings, included_settings_files );
    files[s] = {"settings": settings_files,
                "scores": score_files,
                "instruments": instrument_files,
                "parts": parts_files };
  return files



######################################################################
#   Creating LaTeX files
######################################################################

def write_itex_file (settings, tex_settings, template, filename):
  template = settings.get_template (template);
  basename = tex_settings.get ("basename", "");
  filename = settings.assemble_itex_filename (basename, filename);
  write_file (settings.out_dir, filename, template.render (tex_settings));
  return filename;

def write_texscore_file (settings, tex_settings, template, score):
  template = settings.get_template (template);
  basename = tex_settings.get ("basename", "");
  filename = settings.assemble_texscore_filename (basename, score);
  write_file (settings.out_dir, filename, template.render (tex_settings));
  return filename;

no_criticalcomments_scores = ["Vocal", "Choral", "Organ"];
tex_options_map = {
 "Organ": "vocalscore",
 "Choral": "choralscore",
 "Vocal": "vocalscore",
 "Long": "fullscore",
 "Full": "fullscore",
 "Original": "fullscore",
 "Particell": "vocalscore",
 "InstrumentalParts": "instrumentalparts",
 # TODO: chambermusic
}

def generate_tex_files (settings, lily_files):
  tex_files = [];
  tex_includes = [];
  tex_settings = settings.get_tex_settings ();
  tex_settings["lily_files"] = lily_files;

  score_map = {};
  instruments_scores = [];
  for s in settings.score_names ():
    score_settings = settings.get_score_settings (s);
    instruments_scores.append (score_settings);
    for p in score_settings.get ("scores", []):
      score_map[p] = score_map.get (p, []) + [score_settings];
    for i in tex_settings.get ("instruments", []):
      if i in tex_settings.get ("instrumentalscores", []):
        score_map[i] = score_map.get (i, []) + [score_settings];
  tex_settings["works"] = instruments_scores;
  
  tex_include_templates = settings.get_template_names("TeX_*.itex");
  for t in tex_include_templates:
    base = re.sub( r'TeX_(.*)\.itex', r'\1', t);
    tex_includes.append (write_itex_file (settings, tex_settings, t, base));

  for (score, parts) in score_map.items ():
    this_settings = copy.deepcopy (tex_settings);
    this_settings["scoretype"] = score;
    this_settings["scores"] = parts;
    #this_settings["scorebasetype"] = "Score";
    #this_settings["scorenamebase"] = "ScoreType";
    tmpopts = this_settings.get ("tex_options", [])
    tmpopts.append (tex_options_map.get (this_settings["scoretype"], ""))
    this_settings["tex_options"] = tmpopts
    if "createCriticalComments" not in tex_settings:
      this_settings["createCriticalComments"] = score not in no_criticalcomments_scores;
    this_settings["is_instrument"] = score in tex_settings.get ("instruments", []);
      #this_settings["scorebasetype"] = "Instrument";
      #this_settings["scorenamebase"] = "Name";
    tex_files.append (write_texscore_file (settings, this_settings, "TeX_Score.tex", score))


  # Create the tex instruments file only if we have instruments!
  if set(this_settings.get("instruments",[])) != set(this_settings.get("noscore_instruments",[])):
    this_settings = copy.deepcopy (tex_settings);
    this_settings["scoretype"] = "InstrumentalParts";
    tmpopts = this_settings.get ("tex_options", [])
    tmpopts.append (tex_options_map.get (this_settings["scoretype"], ""))
    this_settings["tex_options"] = tmpopts
    if "createCriticalComments" not in tex_settings:
      this_settings["createCriticalComments"] = score not in no_criticalcomments_scores;
    tex_files.append (write_texscore_file (settings, this_settings, "TeX_Instruments.tex", "Instruments"))
  else:
    print "     No separate instrumental scores, not creating instruments tex file."

  return [tex_files, tex_includes];



######################################################################
#   Creating Makefile
######################################################################

def generate_make_files (settings, lily_files, tex_files):
  make_settings = settings.raw_data.copy ();

  if settings.has_tex ():
    tex_settings = settings.get_tex_settings ();
    tex_settings["includes"] = tex_files[1];
    tex_settings["files"] = tex_files[0];
    make_settings["latex"] = tex_settings;

  score_map = {};
  instruments_scores = [];
  nr = 0;
  for s in settings.score_names ():
    nr += 1;
    score_settings = settings.get_score_settings (s);
    if nr > 1:
      score_settings["nr"] = nr;
    else:
      score_settings["nr"] = "";
    score_settings["srcfiles"] = lily_files[s];
    instruments_scores.append (score_settings);
    for p in score_settings.get ("scores", []):
      score_map[p] = score_map.get (p, []) + [score_settings];
  make_settings["works"] = instruments_scores;

  template = settings.get_template ("Makefile");
  #basename = tex_settings.get ("basename", "");
  file = write_file (settings.out_dir, "Makefile", template.render (make_settings));
  return file;




######################################################################
#   Creating webshop_descriptions.def
######################################################################

def generate_webshop_files (settings, lily_files, tex_files):
  webshop_settings = settings.raw_data.copy ();
  template = settings.get_template ("webshop_descriptions.def");
  #basename = tex_settings.get ("basename", "");
  #print pp.pprint(settings.raw_data);
  #print pp.pprint(settings.score_names ())

  scores=[];

  for s in settings.score_names ():
    score_settings = settings.get_score_settings (s);
    noscore_instruments = score_settings.get ("noscore_instruments", [])
    for i in score_settings.get ("scores", []) + ["Instruments"] + score_settings.get ("instruments", []):
      if i in noscore_instruments:
        continue;
      score_info = score_types.get (i, {});
      score_type = score_info.get ("Name", i);
      score_id = score_info.get ("Number", "XXX");
      try:
        # Replace '1a' to 1.01 (i.e. appended letters will indicate decimals, so they are sorted after 1)
        def chartoindex(matchobj):
          return "%s.%02d" % (matchobj.group(1), ord(matchobj.group(2).lower())-96);
        sid = re.sub(r'^([0-9]+)([a-zA-Z])$', chartoindex, score_id);
        sid = float(sid);
      except ValueError as e:
        sid = score_id;
      scores.append({"id": sid, "sku": score_settings.get ("scorenumber")+"-"+score_id, "type": score_type });
  
  webshop_settings["webshop_editions"] = sorted (scores, key=lambda k: k.get("id", 0));
  
  webshop_settings.update (webshop_settings.get("defaults", {}));
  file = write_file (settings.out_dir, "webshop_descriptions.def", template.render (webshop_settings));



######################################################################
#   Link the orchestrallily package
######################################################################

def generate_oly_link (settings):
  global script_path;
  try:
    os.symlink ("../"+script_path, settings.out_dir+"orchestrallily")
  except OSError:
    pass

######################################################################
#   Main function
######################################################################

def main ():
  settings = Settings ();
  print ("Creating OrchestralLily template in \"%s\", using template \"%s\"." %
         (settings.out_dir, settings.get_template_name () ));

  print ("Creating Lilypond files")
  lily_files = generate_scores (settings);

  tex_files = []
  if settings.has_tex ():
    print ("Creating LaTeX files")
    tex_files = generate_tex_files (settings, lily_files);
  print ("Creating OrchestralLily package link")
  generate_oly_link (settings);
  print ("Creating webshop_descriptions.def")
  generate_webshop_files (settings, lily_files, tex_files);
  print ("Creating Makefile")
  generate_make_files (settings, lily_files, tex_files);

main ();