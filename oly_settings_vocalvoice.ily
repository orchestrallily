\version "2.17.6"
% lilypond settings for vocal scores
#(ly:set-option 'relative-includes #t)
\include "oly_settings_global.ily"
#(ly:set-option 'incipit #f)

\paper {
  ragged-bottom = ##f
  ragged-last-bottom = ##f
}
\layout {
  \context {  \Staff
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
    \override VerticalAxisGroup #'remove-empty = ##f
  }
  \context {  \ChoirStaff
    % No instrument names, they are already in the title/header!
    \remove "Instrument_name_engraver"
    \override VerticalAxisGroup #'remove-empty = ##f
  }
  \context {
    \Staff \RemoveEmptyStaves
    % No instrument names, they are already in the title/header! (Removing
    % the engraver only from the \Staff context is not sufficient!)
    \remove "Instrument_name_engraver"
  }
  \context {
    \Score
    autoBeaming = ##f
  }
}

