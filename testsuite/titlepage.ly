\version "2.13.17"
\include "../orchestrallily.ly"

% #(set-global-staff-size 35)
% \paper {
%   line-width=10\cm
% }

\header { 
  title = "A nice titlepage" 
  titlepagetitle = \markup \center-column { "A really nice" "titlepage"}
  subtitle = "using OrchestralLily"
  composer = "The software author"
%   poet = "Noone else"
  copyright = "No copyright on this trivial example"
  scoretype = "Full Score / Partitur"
  enteredby = "The editor"
  publisher = \markup \center-column { "Edition Kainhofer, 2008" 
                                      \line{Number of the score}}
%   date = "June 2008"
  ensemble = "Soli (SA), Coro e Orchestra o Organo"
  instruments = \markup { \column { 
    \line {"Flauto, 2 Clarinetti/Oboi, 2 Corni,"} 
    \line {"2 Trombe, Timpani, 2 Violini, Viola,"}
    \line {"Violoncello e Contrabbasso"}} }
  subsubtitle = \markup{ \medium \fontsize #1 "Trivial to play" }
}


\paper {
  bookTitleMarkup = \titlePageMarkup
}

\pageBreak


TestIiMusic = { c''1 } 
\createScore #"Test" #'("Ii")
