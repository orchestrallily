\version "2.13.17"
\include "../orchestrallily.ly"

\header { title = "Drum- and RhythmicStaff" }

TestIiMusic = \drummode { crashcymbal4 hihat8 halfopenhihat hh hh hh openhihat }
TestIiiMusic = \relative c'' { c2 c,8 e g c}
TestIiiiMusic = { c4\3 e2 <g c>16 <e g c>8. }

\orchestralScoreStructure #'(
  ("Ii" "DrumStaff" ())
  ("DS" "DrumStaff" ("Ii"))
  ("Iii" "RhythmicStaff" ())
  ("Iiii" "TabStaff" ())
)
\orchestralVoiceTypes #'(
  ("Ii" "DrumVoice")
  ("Iiii" "TabVoice")
)

\createScore #"Test" #'("Ii" "Iii" "Iiii")

\createScore #"Test" #'("DS")
