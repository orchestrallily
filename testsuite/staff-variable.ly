\version "2.13.17"
\include "../orchestrallily.ly"

\header { title = "Defining a staff as a variable" }

TestIiMusic = \relative c' { c4 d e f }
TestIiiMusic = \relative c' { f4 g a b }

TestIiiStaff = \new Staff = "SomeStaffID" 
            \with { \remove Staff_symbol_engraver 
                    \override StaffSymbol #'line-count = #3 }
<< \TestIiiMusic >>

\markuplines \justified-lines {The second staff is not auto-generated, but defined manually in TestIiiStaff!}

\createScore #"Test" #'("Ii" "Iii")

