\version "2.13.17"
\include "../orchestrallily.ly"

\header { 
  title = "Displaying copyright on page 2" 
  copyright = "This is the copyright notice (to be shown on page 2)"
}
\paper {
  paper-height=5\cm
}

#(set-copyright-page 2)

\pageBreak
\markup "Second page (copyright will be shown on this page due to #(set-copyright-page 2)"
