\version "2.13.17"
\include "../orchestrallily.ly"

\header { title = "Using your custom contexts with OrchestralLily" }

% Define our own contexts "SquareStaffGroup" and "ThreeStaff":
\layout{
  \context { \StaffGroup
    \name "SquareStaffGroup"
    \alias StaffGroup
    \description "Staff group with a square bracket"
    systemStartDelimiter = #'SystemStartSquare
    \accepts "Staff"
    \accepts "ThreeStaff"
  }
  \context { \Staff
    \name "ThreeStaff"
    \alias Staff
    \description "Staff with only three lines"
    \override StaffSymbol  #'line-count = #3
  }
  \context { \Voice
    \name "RedVoice"
    \alias Voice
    \description "Voice printed all in red"
    \override NoteHead #'color = #red
    \override Stem #'color = #blue
  }
  % Hook the new contexts into the hierarchy...
  \context { \Staff
    \accepts "RedVoice"
  }
  \context { \Score
    \accepts "SquareStaffGroup"
    \accepts "ThreeStaff"
  }
}
  
% Register the contexts with OrchestralLily, we can use the default handlers!
#(oly:register_staff_type_handler "SquareStaffGroup" oly:staff_group_handler)
#(oly:register_staff_type_handler "ThreeStaff" oly:staff_handler)
#(oly:register_voice_type_handler "RedVoice" oly:voice_handler)

\orchestralScoreStructure #'(
  ("Group" "SquareStaffGroup" ("StaffI" "StaffII"))
  ("StaffI" "ThreeStaff" ())
)
\orchestralVoiceTypes #'(
  ("StaffII" "RedVoice")
)

TestStaffIMusic = \relative c'' { c2 d2 }
TestStaffIIMusic = \relative c'' { b4 b b b }

\createScore #"Test" #'("Group")

