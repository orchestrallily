\version "2.13.17"
\include "../orchestrallily.ly"

TestaIMusic = \relative c' { c1 }
TestbIMusic = \relative c' { d1 }

TestaPieceName = "Piece A"
TestbPieceName = "Piece B"

\markuplines \table-of-contents

\createHeadline "Custom headline (normal markup)"
\markup"Some markup"

\createScore #"Testa" #'("I")

\createHeadline "Another manual headline"

\createScore #"Testb" #'("I")

