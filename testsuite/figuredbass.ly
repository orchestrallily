\version "2.13.17"
\include "../orchestrallily.ly"
\paper { ragged-bottom=##t }


\header { title = "Figured Bass" }

TestIMusic = { c''1 }
TestFBMusic = \figuremode { <6>4 <_+! [7 3-] _ 8>16 s16 s8 s4 s8 <8>  }
TestFBiMusic = \TestFBMusic

\orchestralScoreStructure #'(
  ("staff" "ParallelVoicesStaff" ("I" "FBi"))
  ; if no child is given, the name (here FB) is used for the music
  ("FB" "FiguredBass" ())
  ; equivalent to the line above!
  ("FigB" "FiguredBass" ("FB"))
)
\orchestralVoiceTypes #'(
  ("FBi" "FiguredBass")
)

TestPieceName = "Staff with Figured bass"
\createScore #"Test" #'("staff")

TestPieceName = "Figured bass without staff"
\createScore #"Test" #'("FB")
\createScore #"Test" #'("FigB")

