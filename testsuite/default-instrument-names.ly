\version "2.13.17"
\include "../orchestrallily.ly"
\include "../oly_settings_names.ly"

\header { title = "OrchestralLily defines many instrument names" }


TestSSoloMusic = \relative c'' { c1 }
TestVcBMusic = \relative c { c1 }
TestFagIMusic = \relative c' { c1 }
TestVaMusic = \relative c' { c1 }
TestTrMusic = \relative c' { c1 }


\createScore #"Test" #'("SSolo" "VcB" "FagI" "Va" "Tr")
