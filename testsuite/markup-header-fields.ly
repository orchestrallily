\version "2.13.17"
\include "../orchestrallily.ly"

\header { 
  title="My own title" 
  composer="Someone" 
  piece = "Global piece name"
}

testMarkup = \markup {
  \justify {
    This is a text inserted using the \typewriter{"\\markupWithHeader"} 
    command to allow using header fields 
    (via "\fromproperty #'header:fieldname"). For example, the title field 
    is \fontsize #+3 \fromproperty #'header:title and 
    the composer is \fontsize #+3 \fromproperty #'header:composer
  }
}

% Print out the markups. The \fromproperty #'header:field are now properly
% interpreted.
\markup\markupWithHeader \testMarkup
