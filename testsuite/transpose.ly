\version "2.13.17"
\include "../orchestrallily.ly"

\header { title = "OrchestralLily Transposition test case" }

TestIiMusic = \relative c' { \transposition f \key c \major c4 d e f }
TestIiiMusic = \relative c' { \transposition c \key f \major f4 g a b }
TestIiiiMusic = \relative c' { \transposition f \key c \major c4 d e f }
TestIivMusic = \relative c' { \transposition f \key c \major c4 d e f }

TestIxiMusic = \relative c' { \transposition f \key c \major c4 d e f }
TestIxiiMusic = \relative c' { \transposition c \key f \major f4 g a b }
TestIxiiiMusic = \relative c' { \key c \major c4 d e f }

\markup{"Transpose V1 to g', V3 from f'"}
TestIiTransposeTo = g
TestIiiTransposeFrom = f'
TestIivTransposeFrom = c'
TestIivTransposeTo = g
\createScore #"Test" #'("Ii" "Iii" "Iiii" "Iiv")

\markup{"Transpose everything to f'"}
TestTransposeTo = f'
TestIxiiTransposeTo = \relative c' { c1 }
TestIxiiiTransposeTo = \clef "bass"
\createScore #"Test" #'("Ixi" "Ixii" "Ixiii")

