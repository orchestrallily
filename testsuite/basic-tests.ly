\version "2.13.17"
\include "../orchestrallily.ly"

\header { title = "OrchestralLily Structure test case" }

% TestPieceName = "Test piece"


TestIiMusic = \relative c' { c4 g' c, b' }
TestIiiMusic = \relative c'' { c2 c,8 e g c}
TestIiiLyrics = \lyricmode { Da, da -- da -- da -- da! }
TestIiiiMusic = \relative c { c1 }
TestIivMusic = \relative c' { c2 c2 }
TestIvMusic = \relative c' { d1 }
TestIviMusic = \relative c' { d2 d2 }
TestIviiMusic = \relative c' { e1 }
TestIviiiMusic = \relative c' { \voiceOne e2 e2 }
TestIixMusic = \figuremode { <6>2 <6 _!>2 }
TestIxMusic = \drummode { crashcymbal4 hihat8 halfopenhihat hh hh hh openhihat }
TestIxiMusic = \relative c' { \voiceTwo c2 c,8 e g c}
TestIxiLyrics = \lyricmode { Da, da -- da -- da -- da! }
TestIxiClef = \clef "treble_8"


\orchestralScoreStructure #'(
  ("SG" "StaffGroup" ("Ii" "Iii" "Iiii"))
  ("CS" "ChoirStaff" ("Ii" "Iii" "Iiii"))
  ("GS" "GrandStaff" ("Ii" "Iii" "Iiii"))
  ("Parallel" "ParallelMusic" ("Ii" "Iii" "Iiii"))
  ("Simultaneous" "SimultaneousMusic" ("Ii" "Iii" "Iiii"))
  ("SimNonExist110" "SimultaneousMusic" ("Iiii" "Iv" "nonExI"))
  ("SimNonExist011" "SimultaneousMusic" ("nonExI" "Iiii" "Iv"))
  ("SGNonExist100" "StaffGroup" ("Iiii" "nonEx" "nonExI"))
  ("SGNonExist010" "StaffGroup" ("Iiii" "nonEx" "nonExI"))
  
  ("NG1" "GrandStaff" ("Ii" "Iii"))
  ("NG2" "ChoirStaff" ("Iiii" "NG1"))
  ("NG3" "GrandStaff" ("Iiv" "Iv"))
  ("NG4" "ParallelMusic" ("Ivi" "Ivii"))
  ("NestedGroups" "StaffGroup" ("NG1" "NG2" "NG3" "NG4"))
  
  ("Staff" "Staff" ("Ii"))
  ("Ix" "DrumStaff" ())
  ("ParallelStaff" "ParallelVoicesStaff" ("Iiii" "Ixi"))

  ("Combined2" "PartCombinedStaff" ("Ivi" "Iiv"))
  ("Combined1" "PartCombinedStaff" ("Iiv"))
  ("Combined1x" "PartCombinedStaff" ("Iiv" "something"))
  ("Combinedx" "PartCombinedStaff" ("something"))
    
  ("FigBStaff" "ParallelVoicesStaff" ("Ii" "Iix"))
)
\orchestralVoiceTypes #'(
  ("Ix" "DrumVoice")
  ("Iix" "FiguredBass")
)


\markup{"Single Instrument"}
\createScore #"Test" #'("Ii")

\markup{"Multiple toplevel Instruments"}
\createScore #"Test" #'("Ii" "Iiv" "Iv")



\markup{"Lyrics to voice"}
\createScore #"Test" #'("Iii")

\markup{"Instrument & short instrument name"}
IiInstrumentName = "Instrument 1"
IiShortInstrumentName = "I.1"
TestIiMusic = \relative c' { c4 g' c, b' \break c1 }
\createScore #"Test" #'("Ii")
% reset the values!
IiInstrumentName = ""
IiShortInstrumentName = ""
TestIiMusic = \relative c' { c4 g' c, b' }

\markup {"Clef for instrument"}
IiiiClef = \clef "bass"
\createScore #"Test" #'("Iiii")

\markup {"Settings"}
TestSettings = {\mark\markup{\italic "Slow."}}
\createScore #"Test" #'("Ii")
TestSettings = {}



% Staff groups:
\markup{"ParallelMusic"}
\createScore #"Test" #'("Parallel")
\markup{"SimultaneousMusic"}
\createScore #"Test" #'("Simultaneous")
\markup{"StaffGroup"}
\createScore #"Test" #'("SG")
\markup{"ChoirStaff"}
\createScore #"Test" #'("CS")
\markup{"GrandStaff"}
\createScore #"Test" #'("GS")

\markup "SimultaneousMusic with two existing, one non-existing child"
\createScore #"Test" #'("SimNonExist110")
\createScore #"Test" #'("SimNonExist011")

\markup "Staff Group with one existing, two non-existing children"
\createScore #"Test" #'("SGNonExist100")
\markup "Staff Group with non-existing, existing and non-existing child"
\createScore #"Test" #'("SGNonExist010")


\markup{"Nested Groups"}
\createScore #"Test" #'("NestedGroups")

% Staves
\markup{"Staff"}
\createScore #"Test" #'("Staff")

\markup{"Drum staff"}
\createScore #"Test" #'("Ix")

\markup{"Part-combination"}
\createScore #"Test" #'("Combined2")
\markup{"Part-combination, only one voice given"}
\createScore #"Test" #'("Combined1")
\markup{"Part-combination, only one existing voice"}
\createScore #"Test" #'("Combined1x")
\markup{"Part-combination, no existing voice given"}
\createScore #"Test" #'("Combinedx")

\markup{"Staff with parallel voices"}
\createScore #"Test" #'("ParallelStaff")


\markup{"Staff with figured bass"}
\createScore #"Test" #'("FigBStaff")

