\version "2.13.17"
\include "../orchestrallily.ly"

\header { 
  title="Equally-spaced column, aligned across markups" 
}
% Workaround for the docs (lilypond-book trying to be too smart)
% \paper { ragged-right = ##f line-width=15\cm}

\markup \line {\bold "Text of the \"Kyrie eleison\" (first song in a mass)" }
\markup {
  \line {\columns {
    \column { "Kyrie eleison," }
    \column { "Herr, erbarme dich," }
    \column { "Lord, have mercy," }
  }}
}

\markup \line {\bold "Text of the \"Gloria in excelsis Deo\""}
\markup {
  \line {\columns {
    \column {
      \italic "Gloria in excelsis Deo"
      "Et in terra pax"
      "..."
    }
    \column {
      \italic "Ehre sei Gott in der Höhe"
      "und Friede auf Erden"
      "..."
    }
    \column {
      \italic "Glory be to God on high,"
      "and on earth peace,"
      "..."
    }
  }}
}
