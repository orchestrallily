\version "2.13.17"
\include "../orchestrallily.ly"

\header { title = "OrchestralLily Deprecated types test case" }

TestIiMusic = \relative c' { c2 }
TestIiiMusic = \relative c' { d2 }
TestIiiiMusic = \relative c' { e2 }
TestIivMusic = \relative c' { f2 }
TestIvMusic = \relative c' { g2 }
TestIviMusic = \relative c' { a2 }
TestIviiMusic = \relative c' { b2 }
TestIviiiMusic = \relative c' { c2 }
TestIixMusic = \relative c'' { e2 }
TestIxMusic = \relative c' { c2 }
TestIxiMusic = \relative c' { c2 }

\orchestralScoreStructure #'(
  ("PS" PianoStaff ("Ii" "Iii"))
  ("SG" StaffGroup ("PS" "Iiii"))
  
  ("CS" ChoirStaff ("Iiv" "Iv"))
  ("GS" GrandStaff ("Ivi" "Ivii"))
  ("Parallel" ParallelMusic ("SG" "CS"))
  ("Simultaneous" SimultaneousMusic ("Parallel" "GS"))
  ("ParallelStaff" #f ("Iviii" "Iix"))
  ("Combined" #t ("Ix" "Ixi"))
)

\createScore #"Test" #'("Simultaneous" "ParallelStaff" "Combined")
