\version "2.17.6"
% lilypond input for vocal scores
% changes to the full score settings for vocal scores
#(ly:set-option 'relative-includes #t)
#(ly:set-option 'incipit #t)
\include "oly_settings_choralscore.ily"

% Use 27x19 cm paper, 1.5cm margins.
\paper {
  paper-width = 19\cm
  paper-height = 27\cm

  left-margin = 1.5\cm
  right-margin = 1.5\cm
  line-width = 16\cm
}

% 5 mm staff height (piano should be 5.5, but that will be left as a future exercise)
#(set-global-staff-size 14.5)
% SShortInstrumentName = ""
% AShortInstrumentName = ""
% TShortInstrumentName = ""
% BShortInstrumentName = ""
% PShortInstrumentName = ""
#(ly:set-option 'incipit #f)

\header {
  instrument = \VocalScoreTitle
}