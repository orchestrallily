\version "2.17.6"

#(ly:set-option 'relative-includes #t)
\include "orchestrallily.ily"
\include "oly_settings_names.ily"


% Some spacing settings, need to test them before I can enable them
\layout {
  \context { \Score
    \override BarNumber #'self-alignment-X = #0
    \override BarNumber #'outside-staff-priority = #10
    \override Hairpin #'to-barline = ##t
%     autoAccidentals = #'(Staff (same-octave . 0) (any-octave . 1))
    autoCautionaries = #'()
  }
}
