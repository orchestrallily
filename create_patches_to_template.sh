#!/usr/bin/env bash

mkdir -p patches/

for i in "$@"; do
  bn=${i%.new}
  diff -u $bn.new $bn > patches/$bn.patch
done
