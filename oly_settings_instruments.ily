\version "2.17.6"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   Defaults for instrument names, short names, cue names, etc.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\addInstrumentDefinition #"clarinetti"
  #`((instrumentTransposition . ,(ly:make-pitch 0 5 0))
     (shortInstrumentName . "Cl.")
     (instrumentName . "Clarinetti in A")
     (clefGlyph . "clefs.G")
     (middleCPosition . -6)
     (clefPosition . -2)
     (instrumentCueName . "Cl. (in A)")
     (midiInstrument . "clarinet"))

\addInstrumentDefinition #"clarinetto1"
  #`((instrumentTransposition . ,(ly:make-pitch 0 5 0))
     (shortInstrumentName . "Cl.I")
     (instrumentName . "Clarinetto I in A")
     (clefGlyph . "clefs.G")
     (middleCPosition . -6)
     (clefPosition . -2)
     (instrumentCueName . "Cl. (in A)")
     (midiInstrument . "clarinet"))

\addInstrumentDefinition #"fagotti"
  #`((instrumentTransposition . ,(ly:make-pitch -1 0 0))
     (shortInstrumentName . "Fg.")
     (instrumentName . "Fagotti")
     (clefGlyph . "clefs.F")
     (middleCPosition . 2)
     (clefPosition . 2)
     (instrumentCueName . "Fg.")
     (midiInstrument . "bassoon"))

\addInstrumentDefinition #"oboe1"
  #`((instrumentTransposition . ,(ly:make-pitch -1 0 0))
     (shortInstrumentName . "Ob.I")
     (instrumentName . "Oboe I")
     (clefGlyph . "clefs.G")
     (middleCPosition . -6)
     (clefPosition . -2)
     (instrumentCueName . "Ob.I")
     (midiInstrument . "oboe"))

\addInstrumentDefinition #"oboi"
  #`((instrumentTransposition . ,(ly:make-pitch -1 0 0))
     (shortInstrumentName . "Ob.")
     (instrumentName . "Oboi")
     (clefGlyph . "clefs.G")
     (middleCPosition . -6)
     (clefPosition . -2)
     (instrumentCueName . "Ob.")
     (midiInstrument . "oboe"))
