\version "2.17.6"
% lilypond input for full scores
% changes to the global score settings for full scores
#(ly:set-option 'relative-includes #t)
#(ly:set-option 'incipit #t)
\include "oly_settings_global.ily"

#(set-global-staff-size 13)

olySlashSeparator = \markup {
  \center-align
  \vcenter \combine
  \beam #3.0 #0.5 #0.48
  \raise #1.25 \beam #3.0 #0.5 #0.48
}

\paper {
  system-separator-markup = \olySlashSeparator
  #(define (page-post-process layout pages) (oly:create-toc-file layout pages))
  system-system-spacing #'minimum-distance = #20
}

\layout {
  \context {
      \Score
      \override VerticalAlignment #'max-stretch = #10000000
  }
}
