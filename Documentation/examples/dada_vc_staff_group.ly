\version "2.11.40"
\include "dada-defs.ly"
\paper { line-width=11\cm paper-height=8.8\cm paper-width=12\cm }
#(set-global-staff-size 18)

\orchestralScoreStructure #'(
  ("VcB" StaffGroup ("Vc"))
  ("FullScore" ParallelMusic ("Percussion" "SSolo" "VcB"))
)

\createScore #"Dada" #'("FullScore")

