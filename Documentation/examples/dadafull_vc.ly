\version "2.11.40"
\include "dadafull-defs.ly"
\paper { paper-height=8.5\cm }

\header { instrument = \VcInstrumentName }

\createScore #"Dada" #'("Vc")
\createScore #"Didi" #'("Vc")
\createScore #"Dodo" #'("Vc")