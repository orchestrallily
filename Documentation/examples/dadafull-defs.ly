\version "2.11.40"
\include "orchestrallily.ly"
% \paper { line-width=10\cm }
% #(set-global-staff-size 14)

\header { title = "A useless opus" }

TimKey = \key c \major
TimClef = \clef "bass"
TimInstrumentName = "Timpani"
TimShortInstrumentName = "Tim."
VcClef = \clef "bass"
VcInstrumentName = "Violoncello"
VcShortInstrumentName = "Vc."
SSoloInstrumentName = "Soprano Solo"
SSoloShortInstrumentName = "S."

DadaPieceName = "1) Dada song"
DadaPieceNameTacet = "1) Dada song - Tacet"
DadaSettings = {\mark\markup{\italic "Slow."}}
DadaVcMusic = \relative c { c4 g' c, b' }
DadaSSoloMusic = \relative c'' {c2 c,8 e g c}
DadaSSoloLyrics = \lyricmode { Da, da -- da -- da -- da! }

DidiPieceName = "2) Didi song"
DidiPieceNameTacet = "2) Didi song - Tacet"
DidiKey = \key fis \major
DidiTimMusic = \relative c { g1\startTrillSpan~ | g1\stopTrillSpan }
DidiSSoloMusic = \relative c' { fis8 cis'4. fis,8 cis'4. | fis8 cis'4. fis,8 cis'4.}
DidiSSoloLyrics = \lyricmode { Di -- di, di -- di, di -- di, di -- di! }

DodoPieceName = "3) Dodo song"
DodoPieceNameTacet = "3) Dodo song - Tacet"
DodoTimMusic = \relative c { c1\<\startTrillSpan~ | c1\!\stopTrillSpan\ff }
DodoVcMusic = \relative c { c1:16\< | c1:16\!\ff }
DodoSSoloMusic = \relative c' { c4 d8 e f g a b | c1\ff }
DodoSSoloLyrics = \lyricmode { Do, do, do, do, do, do, do, do... }


\orchestralScoreStructure #'(
  ("FullScore" ParallelMusic ("Tim" "SSolo" "VcB"))
  ("VcB" StaffGroup ("Vc"))
; a nested staff to highlight nested groups:
  ("NestedScore" StaffGroup ("TimGr" "Solo" "VcB"))
  ("TimGr" GrandStaff ("Tim"))
  ("Solo" ChoirStaff ("SSolo"))
)
