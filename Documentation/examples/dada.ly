\version "2.11.40"
\include "dada-defs.ly"

% the soprano score:
\createScore #"Dada" #'("SSolo")

% the Vc score:
\createScore #"Dada" #'("Vc")

% the percussion score, no notes defined -> Tacet
\createScore #"Dada" #'("Percussion")

% the full score:
\createScore #"Dada" #'("FullScore")
