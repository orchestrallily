\version "2.11.40"
\include "dadafull-defs.ly"
\paper { paper-height=8.5\cm }

\header { instrument = \TimInstrumentName }

\createScore #"Dada" #'("Tim")
\createScore #"Didi" #'("Tim")
\createScore #"Dodo" #'("Tim")