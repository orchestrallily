\version "2.11.40"
\include "dadafull-defs.ly"
\paper { paper-height=11\cm }

\header { instrument = \SSoloInstrumentName }

\createScore #"Dada" #'("SSolo")
\createScore #"Didi" #'("SSolo")
\createScore #"Dodo" #'("SSolo")