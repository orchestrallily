\version "2.11.40"
\include "dada-defs.ly"
\paper { line-width=11\cm paper-height=10\cm paper-width=12\cm }
#(set-global-staff-size 18)


\header { 
  copyright =""
  instrument = \VcInstrumentName
}

% For the SSolo staff, use a smaller staff size!
DadaSSoloSettings = {
    \DadaSettings 
    \set fontSize = #-5
    \override Staff.StaffSymbol #'staff-space = #(magstep -5)
}

\createScore #"Dada" #'("InvertedScore")
