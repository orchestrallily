\version "2.11.41"

\header { Title = "Full score structure" }
#(set-global-staff-size 14)
\layout {
%   \context { \GrandStaff \consists "Instrument_name_engraver" }
%   \context { \StaffGroup \consists "Instrument_name_engraver" }
  \context {
      \StaffGroup
      % If only one non-empty staff in a system exists, still print the backet
      \override SystemStartBracket #'collapse-height = #1
  }
}
\paper {
  ragged-right = ##t
}

\score {
<<
  \context StaffGroup = "WoodStaff" \with { instrumentName="Wood" } <<
%     \new GrandStaff \with { instrumentName="Fl" } <<
%       \new Staff \with {instrumentName="FlI"} {c''1} 
%       \new Staff \with {instrumentName="FlII"} {c''1} 
   \new Staff \with {instrumentName="Fl"} {\partcombine {c''1}{c'1}}
%     >>
%     \new GrandStaff \with { instrumentName="Ob" } <<
    \new Staff \with {instrumentName="Ob"} {\partcombine {c''1}{c'1}} 
%       \new Staff \with {instrumentName="ObI"} {c''1} 
%       \new Staff \with {instrumentName="ObII"} {c''1} 
%     >>
  >>
%   \context StaffGroup = "BrassStaff" \with { instrumentName="Brass" } <<
%     \new GrandStaff \with { instrumentName="Cor" } <<
%       \new Staff \with {instrumentName="CorI"} {c''1} 
%       \new Staff \with {instrumentName="CorII"} {c''1} 
%     >>
%     \new GrandStaff \with { instrumentName="Tr" } <<
%       \new Staff \with {instrumentName="TrI,II"} {\partcombine {c''1}{ c'1} }
%       \new Staff \with {instrumentName="TrIII"} {c''1} 
%     >>
%   >>
  \context StaffGroup = "StringsStaff" \with { instrumentName="Strings" } <<
    \new GrandStaff \with { instrumentName="V" } <<
      \new Staff \with {instrumentName="VI"} {c''1} 
      \new Staff \with {instrumentName="VII"} {c''1} 
    >>
    \new Staff \with {instrumentName="Va"} {c''1} 
  >>
  \new Staff \with {instrumentName="SSolo"} {c''1} 
  \new Staff \with {instrumentName="TSolo"} {c''1} 
%   \new Staff \with {instrumentName="BSolo"} {c''1} 
  \new ChoirStaff = "ChoirStaff" \with { instrumentName="Ch" } <<
    \new Staff \with {instrumentName="S"} {c''1} 
    \new Staff \with {instrumentName="A"} {c''1} 
    \new Staff \with {instrumentName="T"} {\clef "treble_8" c'1} 
    \new Staff \with {instrumentName="B"} {\clef "bass" c1} 
  >>
  \context StaffGroup = "VcBStaff" <<
    \new Staff \with { instrumentName="VcB" } {\clef "bass" c1} 
  >>
>>
}