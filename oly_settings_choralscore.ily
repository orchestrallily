\version "2.17.6"
% lilypond input for Choral scores: A4, 5mm staff height
#(ly:set-option 'relative-includes #t)
#(ly:set-option 'incipit #t)
\include "oly_settings_fullscore.ily"
#(ly:set-option 'incipit #f)

% Don't print short instruments name, they are clear from the beginning
OShortInstrumentName = \markup\null
OIShortInstrumentName = \markup\null
OIIShortInstrumentName = \markup\null
OrganShortInstrumentName = \markup\null
BCShortInstrumentName = \markup\null
PfeShortInstrumentName = \markup\null
SShortInstrumentName = \markup\null
AShortInstrumentName = \markup\null
TShortInstrumentName = \markup\null
BShortInstrumentName = \markup\null

SSoloShortInstrumentName = \markup\null
ASoloShortInstrumentName = \markup\null
TSoloShortInstrumentName = \markup\null
BSoloShortInstrumentName = \markup\null

OIInstrumentName = \markup\null
OIIInstrumentName = \markup\null

% A4 with 1.5 cm margin. The system bracket will not count towards the margin,
% so the left margin will be 1.5 mm larger!
\paper {
  paper-width = 21\cm
  paper-height = 29.7\cm
  left-margin = 1.65\cm
  right-margin = 1.5\cm
  line-width = 17.85\cm
  system-system-spacing #'minimum-distance = #2

  system-separator-markup = #f
}

% Create MIDI and PDF!
% \setCreateMIDI ##t
% \setCreatePDF ##t

% 5 mm staff height
#(set-global-staff-size 14.5)

\header {
  instrument = \ChoralScoreTitle
}