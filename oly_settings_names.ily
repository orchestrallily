\version "2.17.6"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   Defaults for instrument names, short names, cue names, etc.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Clef definitions, either old-style (using the Breitkopf style) or new style
FlClef = "treble"
FlIClef = \FlClef
FlIIClef = \FlClef
ObClef = "treble"
ObIClef = \ObClef
ObIIClef = \ObClef
ClIClef = "treble"
ClIIClef = \ClIClef
FagClef = "bass"
FagIClef = \FagClef
FagIIClef = \FagClef
FagTenorClef = "tenor"
CFagClef = "bass"
CorClef = "treble"
CorIClef = \CorClef
CorIIClef = \CorClef
TrbClef = "tenor"
TrbIClef = \TrbClef
TrbIIClef = \TrbClef
TrbIIIClef = "bass"
TbeClef = "treble"
TbeIClef = \TbeClef
TbeIIClef = \TbeClef
ClniClef = "treble"
ClnoIClef = \ClniClef
ClnoIIClef = \ClniClef
TimClef = "bass"
VClef = "treble"
VIClef = \VClef
VIIClef = \VClef
VaClef = "alto"
VaIClef = \VaClef
VaIIClef = \VaClef
VaSoloClef = \VaClef
VcBClef = "bass"
VcClef = \VcBClef
VcTenorClef = "tenor"
CbClef = \VcBClef
SClef = "treble"
AClef = "treble"
TClef = "treble_8"
TIClef = \TClef
TIIClef = \TClef
BClef = "bass"
BIClef = \BClef
BIIClef = \BClef
SingstimmeClef = "treble"
SSoloClef = \SClef
ASoloClef = \AClef
TSoloClef = \TClef
BSoloClef = \BClef
OIClef = "treble"
OIIClef = "bass"
OIaClef = "treble"
OIIaClef = "bass"
OIbClef = "treble"
OIIbClef = "bass"
BCClef = "bass"
PIClef = "treble"
PIIClef = "bass"



FlInstrumentName = "Flauti"
FlIInstrumentName = "Flauto I"
FlIIInstrumentName = "Flauto II"
ObInstrumentName = "Oboi"
ObIInstrumentName = "Oboe I"
ObIIInstrumentName = "Oboe II"
ClInstrumentName = "Clarinetti"
ClIInstrumentName = "Clarinetto I"
ClIIInstrumentName = "Clarinetto II"
FagInstrumentName = "Fagotti"
FagIInstrumentName = "Fagotto I"
FagIIInstrumentName = "Fagotto II"
CFagInstrumentName = "Contrafagotto"
CorInstrumentName = "Corni"
CorIInstrumentName = "Corno I"
CorIIInstrumentName = "Corno II"
TrbInstrumentName = "Tromboni"
TrbIInstrumentName = "Trombone I"
TrbIIInstrumentName = "Trombone II"
TrbIIIInstrumentName = "Trombone III"
TbeInstrumentName = "Trombe"
TbeIInstrumentName = "Tromba I"
TbeIIInstrumentName = "Tromba II"
ClniInstrumentName = "Clarini"
ClnoIInstrumentName = "Clarino I"
ClnoIIInstrumentName = "Clarino II"
TimInstrumentName = "Timpani"
VInstrumentName = "Violino"
VIInstrumentName = "Violino I"
VIIInstrumentName = "Violino II"
VaInstrumentName = "Viola"
VaIInstrumentName = "Viola I"
VaIIInstrumentName = "Viola II"
VaSoloInstrumentName = "Viola Solo"
VcBInstrumentName = \markup { \override #'(baseline-skip . 2.2) \center-column { "Violoncello e" "Contrabbasso"}}
VcInstrumentName ="Violoncello"
CbInstrumentName ="Basso"
% ChInstrumentName = "Coro"
SingstimmeInstrumentName = "Singstimme"
SInstrumentName = "Soprano"
AInstrumentName = "Alto"
TInstrumentName = "Tenore"
TIInstrumentName = "Tenore I"
TIIInstrumentName = "Tenore II"
BInstrumentName = "Basso"
BIInstrumentName = "Basso I"
BIIInstrumentName = "Basso II"
SSoloInstrumentName = "Soprano Solo"
TSoloInstrumentName = "Tenore Solo"
BSoloInstrumentName = "Basso Solo"
ASoloInstrumentName = "Alto Solo"
OInstrumentName = "Organo"
OIInstrumentName = "Organo I"
OIIInstrumentName = "Organo II"
BCInstrumentName = \markup{\column{"Basso" "Continuo"}}
OrganInstrumentName = "Organo"
ContinuoInstrumentName = \OrganInstrumentName
PInstrumentName = "Piano"
ChInstrumentTitle = "Coro"

OriginalScoreTitle = "Originalpartitur"
FullScoreTitle = "Partitur / Full score"
LongScoreTitle = "Partitur / Full score"
OrganScoreTitle = "Orgelauszug / Organ score"
VocalScoreTitle = \markup{\center-column{"Klavierauszug" "Vocal score"}}
ParticellTitle = "Vokalparticell / Vocal particell"
ChoralScoreTitle = \markup{\center-column{"Chorstimmen" "Choral score"}}
SoloChoralScoreTitle = \markup{\center-column {"Chorstimmen mit Soli" "Choral score with solo parts"}}
SoloScoreTitle = "Solostimmen / Solo parts"


FlShortInstrumentName = "Fl."
FlIShortInstrumentName  = "Fl. I"
FlIIShortInstrumentName  = "Fl. II"
ObShortInstrumentName  = "Ob."
ObIShortInstrumentName  = "Ob. I"
ObIIShortInstrumentName  = "Ob. II"
ClShortInstrumentName = "Cl."
ClIShortInstrumentName = "Cl.I"
ClIIShortInstrumentName = "Cl.II"
FagShortInstrumentName  = "Fag."
FagIShortInstrumentName  = "Fag. I"
FagIIShortInstrumentName  = "Fag. II"
CFagShortInstrumentName  = "Cfag."
CorShortInstrumentName = "Cor."
CorIShortInstrumentName = "Cor.I"
CorIIShortInstrumentName = "Cor.II"
TrbShortInstrumentName  = "Trb."
TrbIShortInstrumentName  = "Trb. I"
TrbIIShortInstrumentName  = "Trb. II"
TrbIIIShortInstrumentName  = "Trb. III"
TbeShortInstrumentName = "Tbe."
TbeIShortInstrumentName = "Tbe.I"
TbeIIShortInstrumentName = "Tbe.II"
ClniShortInstrumentName = "Clni."
ClnoIShortInstrumentName = "Clno.I"
ClnoIIShortInstrumentName = "Clno.II"
TimShortInstrumentName = "Tim."
VShortInstrumentName = "Vl."
VIShortInstrumentName = "V.I"
VIIShortInstrumentName = "V.II"
VaShortInstrumentName = "Va."
VaIShortInstrumentName = "Va.I"
VaIIShortInstrumentName = "Va.II"
VaSoloShortInstrumentName = "Va.Solo"
VcBShortInstrumentName = \markup{\column{"Vc." "e B."}}
VcShortInstrumentName  = "Vc."
CbShortInstrumentName  = "B."
% ChShortInstrumentName = "C."
SShortInstrumentName = "S."
% SShortInstrumentName = ##f
AShortInstrumentName = "A."
TShortInstrumentName = "T."
TIShortInstrumentName = "T.1"
TIIShortInstrumentName = "T.2"
BShortInstrumentName = "B."
BIShortInstrumentName = "B.1"
BIIShortInstrumentName = "B.2"
SSoloShortInstrumentName  = "S.Solo"
ASoloShortInstrumentName  = "A.Solo"
TSoloShortInstrumentName  = "T.Solo"
BSoloShortInstrumentName  = "B.Solo"
OShortInstrumentName = "Org."
OIShortInstrumentName = "Org. I"
OIIShortInstrumentName = "Org. II"
OrganShortInstrumentName = "Org."
BCShortInstrumentName = "B.C."
ContinuoShortInstrumentName = \OrganShortInstrumentName



cueFl = "Fl."
cueFlI = "Fl.I"
cueFlII = "Fl.II"
cueCl = "Clt."
cueClI = "Clt.I"
cueClII = "Clt.II"
cueObI = "Ob.I"
cueObII = "Ob.II"
cueOb = "Ob."
cueFagI = "Fag.I"
cueFagII = "Fag.II"
cueFag = "Fag."
cueCor = "Cor."
cueCorI = "Cor.I"
cueCorII = "Cor.II"
cueTbe = "Tbe."
cueTbeI = "Tbe.I"
cueTbeII = "Tbe.II"
cueClni = "Clni."
cueClnoI = "Clno.I"
cueClnoII = "Clno.II"
cueTrb = "Trb."
cueTrbI = "Trb.I"
cueTrbII = "Trb.II"
cueTrbIII = "Trb.III"
cueTim = "Tim."
cueV = "Vl."
cueVI = "Vl.I"
cueVII = "Vl.II"
cueVa = "Va."
cueVaI = "Va.I"
cueVaII = "Va.II"
cueVaSolo = "Va.Solo"
cueVcB = "Vc./B."
cueVc = "Cello"
cueCb = "Basso"
cueArchi = "Archi"
cueCh = "Coro"
cueS = "S."
cueA = "A."
cueT = "T."
cueTI = "T.1"
cueTII = "T.2"
cueB = "B."
cueBI = "B.1"
cueBII = "B.2"
cueSSolo = "S.Solo"
cueASolo = "A.Solo"
cueTSolo = "T.Solo"
cueBSolo = "B.Solo"
cueO = "Org."
cueBC = "B.C."

senzaCb = \cueText "senza Cb."
senzaVcB = \cueText "senza Vc/Cb."
senzaVcBstacked = \cueMarkup \markup{\override #'(baseline-skip . 2) \column{ senza Vc/Cb.}}
conCb = \cueText "con Cb."
conVcB = \cueText "con Vc/Cb."
conVcBstacked = \cueMarkup \markup{\override #'(baseline-skip . 2) \column{ con Vc/Cb.}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   SCORE NUMBERS FOR PUBLISHING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OriginalScoreNumber = "0"
FullScoreNumber = "1"
LongScoreNumber = "1a"
VocalScoreNumber = "2"
OrganScoreNumber = "2"
ParticellNumber = "3"

ChoralScoreNumber = "10"
ChNumber = "10"
SNumber = "11"
ANumber = "12"
TNumber = "13"
TINumber = "13a"
TIINumber = "13b"
BNumber = "14"
BINumber = "14a"
BIINumber = "14b"
SingstimmeNumber = "15"
SoloChoralScoreNumber = "15a"
SoloScoreNumber = "15"
SSoloNumber = "16"
ASoloNumber = "17"
TSoloNumber = "18"
BSoloNumber = "19"

ONumber = "20"
OrganNumber = \ONumber
OINumber = "20"
OIINumber = "21"
PNumber = "20a"
BCNumber = "21"
ContinuoNumber = \BCNumber

InstrumentsNumber = "25"
VNumber = "30"
VINumber = "30"
VIINumber = "31"
VaNumber = "32"
VaSoloNumber = "32a"
VcBNumber = "33"
VcNumber = "34"
CbNumber = "35"

FlNumber = "40"
FlINumber = "40"
FlIINumber = "41"
ObNumber = "42"
ObINumber = "42"
ObIINumber = "43"
ClNumber = "44"
ClINumber = "44"
ClIINumber = "45"
FagNumber = "46"
FagINumber = "46"
FagIINumber = "47"
CFagNumber = "48"

CorNumber = "50"
CorINumber = "50"
CorIINumber = "51"
TbeNumber = "52"
TbeINumber = "52"
TbeIINumber = "53"
ClniNumber = \TbeNumber
ClnoINumber = \TbeINumber
ClnoIINumber = \TbeIINumber
TrbNumber = "54"
TrbINumber = "54"
TrbIINumber = "55"
TrbIIINumber = "56"
TbaNumber = "57"

TimNumber = "60"
ArpaNumber = "65"


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%   SCORE NUMBERS FOR PUBLISHING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FlMidiInstrument = "flute"
FlIMidiInstrument = "flute"
FlIIMidiInstrument = "flute"
ObMidiInstrument = "oboe"
ObIMidiInstrument = "oboe"
ObIIMidiInstrument = "oboe"
ClMidiInstrument = "clarinet"
ClIMidiInstrument = "clarinet"
ClIIMidiInstrument = "clarinet"
FagMidiInstrument = "bassoon"
FagIMidiInstrument = "bassoon"
FagIIMidiInstrument = "bassoon"
CFagMidiInstrument = "bassoon"
CorMidiInstrument = "french horn"
CorIMidiInstrument = "french horn"
CorIIMidiInstrument = "french horn"
TrbMidiInstrument = "trombone"
TrbIMidiInstrument = "trombone"
TrbIIMidiInstrument = "trombone"
TrbIIIMidiInstrument = "trombone"
TbeMidiInstrument = "trumpet"
TbeIMidiInstrument = "trumpet"
TbeIIMidiInstrument = "trumpet"
ClniMidiInstrument = "trumpet"
ClnoIMidiInstrument = "trumpet"
ClnoIIMidiInstrument = "trumpet"
TimMidiInstrument = "timpani"
VMidiInstrument = "violin"
VIMidiInstrument = "violin"
VIIMidiInstrument = "violin"
% VaMidiInstrument = "viola"
% VaIMidiInstrument = "viola"
% VaIIMidiInstrument = "viola"
VaMidiInstrument = "violin"
VaIMidiInstrument = "violin"
VaIIMidiInstrument = "violin"
VaSoloMidiInstrument = "violin"
VcBMidiInstrument = "cello"
VcMidiInstrument ="cello"
CbMidiInstrument ="contrabass"

SMidiInstrument = "accoustic grand"

% ChMidiInstrument = "choir aahs"
% SMidiInstrument = "synth voice"
% AMidiInstrument = "choir aahs"
% TMidiInstrument = "voice oohs"
% BMidiInstrument = "synth voice"
SMidiInstrument = "voice aahs"
AMidiInstrument = \SMidiInstrument
TMidiInstrument = "voice oohs"
BMidiInstrument = "voice oohs"
SMidiInstrument = "synth voice"
AMidiInstrument = "choir aahs"
TMidiInstrument = "voice oohs"
TIMidiInstrument = \TMidiInstrument
TIIMidiInstrument = \TMidiInstrument
BMidiInstrument = "synth voice"
BIMidiInstrument = \BMidiInstrument
BIIMidiInstrument = \BMidiInstrument
SSoloMidiInstrument = "synth voice"
TSoloMidiInstrument = "synth voice"
BSoloMidiInstrument = "synth voice"
ASoloMidiInstrument = "synth voice"
OMidiInstrument = "church organ"
RealizedContinuoMidiInstrument = "church organ"